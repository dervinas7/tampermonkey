// ==UserScript==
// @name         CoinMarketcap
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Cambia el título en las páginas de CoinMarketcap, eliminando 'Precio, gráficos, capitalización de mercado de ' y ' | CoinMarketCap'
// @author       Marco Antonio Arias Antolín
// @match        https://coinmarketcap.com/es/currencies/*
// @icon         https://www.google.com/s2/favicons?domain=coinmarketcap.com
// @grant        none
// ==/UserScript==

/*
Cambia:
    'Precio, gráficos, capitalización de mercado de Moneda de Ejemplo | CoinMarketCap'
    por
    'Moneda de Ejemplo'
Cada vez que se cambia el título.
*/
(function() {
    'use strict';

    var title = document.title.split('mercado de ')[1].split(' | CoinMarket')[0];
    document.title = title;
    new MutationObserver(function(evt) {
        if (document.title != title) document.title = title;})
        .observe(document.querySelector('title'),{ childList: true });
})();
