// ==UserScript==
// @name         PVU
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Herramienta para calcular las producciones/hora y producciones/hora*precio en el juego PVU (Plants vs Undead)
// @author       Marco Antonio Arias Antolín
// @match        https://marketplace.plantvsundead.com
// @icon         https://www.google.com/s2/favicons?domain=plantvsundead.com
// @grant        unsafeWindow
// ==/UserScript==

/*
Modo de empleo:
    Actualizar en la página del mercado.
    Actualizar la página (F5)
    Pulsar en botón "Calcular" en cada página del mercado.
*/
unsafeWindow.calcular = calcular;
function calcular() {
    let lista = document.querySelectorAll('li[data-v-7cf00384] .tw-text-center');
    let produccion = [];
    let precios = [];
    var maxProduccion = 0;
    var maxPpp = 0; // produccion/precio
    lista.forEach((plant) => {
        let planta = plant.innerText.replace('LE: ', '').replace(' Hour', '').split('/');
        let le = planta[0];
        let tiempo = planta[1];
        let valor = parseInt(le)/parseInt(tiempo);
        if (valor > maxProduccion) {
            maxProduccion = valor;
        }
        produccion.push(valor);
    });
    document.querySelectorAll('div[data-v-d31307be]>img+p[data-v-d31307be]').forEach(p => {
        p.style.background = "black";
        p.style.padding = "6px";
        let leh = produccion.shift()
        let precio = leh/parseFloat(p.innerHTML);
        if (precio > maxPpp) {
            maxPpp = precio;
        }
        produccion.push(leh);
        precios.push(precio);
    })
    document.querySelectorAll('p[data-v-d31307be].tw-text-right').forEach((plant) => {
        let leh = produccion.shift();
        let precio = precios.shift();
        plant.style.background = 'none';
        plant.style.fontWeight = 'normal';
        plant.style.color = 'lightgreen';
        plant.style.fontSize = '16px';
        plant.parentNode.parentNode.parentNode.parentNode.style.background = 'rgba(0, 0, 0, 0.1)';
        plant.parentNode.parentNode.style.background = 'none';
        if (leh == maxProduccion) {
            plant.parentNode.parentNode.parentNode.parentNode.style.background = 'darkgreen';
            plant.style.background = 'black';
            plant.style.fontWeight = 'bolder';
            plant.style.padding = '6px';
        }
        if (precio == maxPpp) {
            plant.parentNode.parentNode.style.background = 'orange';
            plant.parentNode.parentNode.style.padding = '6px';
        }
        plant.innerHTML = `${leh.toFixed(2)}<br>${precio.toFixed(2)}`;
    });
}

(function() {
    'use strict';

    window.onload = function() {
        setTimeout(function(){
            document.getElementsByClassName("sm:tw-rounded-3xl")[0].insertAdjacentHTML('afterbegin', '<button id="boton" onclick="calcular()">Calcular</button>');
            document.getElementById("boton").style.color = 'lightgreen';
            document.getElementById("boton").style.padding = '6px';
            document.getElementById("boton").style.border = '2px solid lightgreen';
        }, 1000);
    }
})();
