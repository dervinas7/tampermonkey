// ==UserScript==
// @name         CryptoRoyale
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  ¡¡NO ES UN BOT!! Calcula las ganancias totales, por sesión y por hora en ROY y Dólares
// @author       Marco Antonio Arias Antolín
// @match        https://cryptoroyale.one/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

/*
¡¡NO ES UN BOT!!
Sirve para calculas las ganancias totales, por sesión y por hora en ROY y Dólares.
Coge el precio de la página y la página sólo lo actualiza cuando se actualiza la página.
Si quieres saber el calculo actualizado actualiza la página (F5).
Se pierden los datos de sesión al actualizar la página.
*/
var start = new Date();
var royInicial = null;

(function() {
    'use strict';

    setInterval(function() {
        $(".cardList").click();

        // beneficio hora
        if(!royInicial) {
            royInicial = parseFloat($("#left b").html());
        } else {
            var t = (new Date() - start) / (1000 * 60 * 60);
            var royActual = parseFloat($("#left b").html());
            var roy = royActual - royInicial;
            var royh = roy / t;
            var tiempo = new Date(new Date() - start);
            var horas = ("0" + tiempo.getUTCHours()).slice (-2);
            var minutos = ("0" + tiempo.getMinutes()).slice (-2);
            var segundos = ("0" + tiempo.getSeconds()).slice (-2);
            var transcurrido = horas + ":" + minutos + ":" + segundos;
            var precio = parseFloat(document.getElementsByClassName("container")[0].children[0].children[2].children[0].innerText.split('$')[1]);
            var info = `A: ${royActual.toFixed(2)} ROY - $${(royActual * precio).toFixed(2)}<br>
                S: ${roy.toFixed(2)} ROY - $${(precio*roy).toFixed(2)}<br>
                H: ${royh.toFixed(2)} ROY - $${(precio*royh).toFixed(2)}<br>
                T: ${transcurrido}`;
            if(!$("#roy").length) {
                $(".future-timeline-right").children().first().append(`<span id="roy"></span>`); //`<button onclick="start = new Date(); royInicial = parseFloat($('#left b').html());">RESET</button>`);
                $("#roy").html(info);
                document.styleSheets[0].insertRule(`#roy {
                        background: lightgrey;
                        border: 2px solid black;
                        color: black;
                        display: block;
                        font-family: Courier;
                        font-size: 16px;
                        font-weight: bold;
                        padding: 6px;
                    }`, 0);
            } else {
                $("#roy").html(info);
            }
        }
    }, 1000);

})();
